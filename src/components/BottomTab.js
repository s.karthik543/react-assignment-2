import React, { PureComponent, Component } from 'react';
import {
    View,
    Text,
    Image,
    StyleSheet,
    TouchableOpacity
} from 'react-native';
import PropTypes from 'prop-types';

import Colors from '../utils/Colors';

class BottomTab extends Component {

    render() {
        const { isSelected, onTabPress, title } = this.props;

        return (
            <TouchableOpacity
                onPress={() => onTabPress(title)}
                activeOpacity={1.0}
                style={styles.container} >

                {isSelected && <View style={styles.selectedItemContainer}>
                    {this.renderOptions}
                    <View style={styles.selector} />
                </View>}

                {!isSelected && this.renderOptions}
            </TouchableOpacity>
        )
    }

    get renderOptions() {
        const { icon, title } = this.props;
        return (
            <>
                <Image source={icon} />
                <Text style={{ fontSize: 12, marginTop: 4, fontWeight: '700', color: Colors.BLACK }}>
                    {title}
                </Text>
            </>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    selector: {
        width: 6,
        height: 6,
        borderRadius: 3,
        backgroundColor: Colors.YELLOW,
        marginTop: 5
    },
    selectedItemContainer: {
        position: 'absolute',
        top: -20,
        left: 0,
        right: 0,
        bottom: -20,
        backgroundColor: Colors.WHITE,
        borderRadius: 50,
        paddingTop: 10,
        alignItems: 'center'
    }
});

BottomTab.propTypes = {
    isSelected: PropTypes.bool.isRequired,
    title: PropTypes.string.isRequired,
    icon: PropTypes.node.isRequired
}

export default BottomTab;