import React, { PureComponent } from 'react';
import {
    View,
    Image,
    StyleSheet,
    TouchableOpacity
} from 'react-native';

const home = require('../../assets/images/ic_home.png');
const profile = require('../../assets/images/ic_avatar.png');

class Toolbar extends PureComponent {

    render() {

        return (
            <View style={styles.container}>

                <Image resizeMode='contain'
                    style={styles.home}
                    source={home} />

                <Image style={styles.profile} source={profile} />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        height: 56,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    backButton: {
        marginLeft: 16
    },
    backImage: {
        width: 32,
        height: 32
    },
    home: {
        marginLeft: 16,
        width: 32,
        height: 32,
    },
    profile: {
        width: 48,
        height: 48,
        marginRight: 16,
    }
});

export default Toolbar;