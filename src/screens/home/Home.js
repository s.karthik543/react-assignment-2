import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    StyleSheet,
    SafeAreaView,
    TouchableOpacity
} from 'react-native';

import Toolbar from '../../components/Toolbar';
import BottomTab from '../../components/BottomTab';
import Colors from '../../utils/Colors';
import Messages from '../../utils/Messages';

const policy = require('../../../assets/images/ic_policy.png');
const apply = require('../../../assets/images/ic_apply.png');
const claim = require('../../../assets/images/ic_claim.png');
const account = require('../../../assets/images/ic_account.png');
const rightArrow = require('../../../assets/images/ic_right_arrow.png');

class Home extends Component {

    constructor(props) {
        super(props);
        this.state = {
            selectedTab: Messages.POLICIES
        }
    }

    onTabSelected = (selectedTab) => {
        this.setState({ selectedTab })
    }

    render() {
        const { selectedTab } = this.state;

        return (
            <SafeAreaView style={styles.container}>
                <Toolbar />

                <TouchableOpacity activeOpacity={0.9} style={styles.addMoreProtectionButton}>
                    <Text style={styles.addMoreProtectionButtonLabel}>
                        Add more protection
                    </Text>
                </TouchableOpacity>

                <View style={styles.cardContainer}>

                    <View style={styles.expireContainer}>
                        <Text style={styles.expireLabel} >
                            Expires 19 June 2020
                        </Text>
                        <Image source={rightArrow} style={styles.arrowIcon} />
                    </View>

                    <Text style={styles.paymentDueLabel} >
                        Next payment due
                    </Text>
                    <Text style={styles.paymentDueDate} >
                        19 September 2019
                    </Text>

                    <View style={styles.descriptionContainer}>
                        <Text style={styles.singleHealthLabel} >
                            Single Health
                        </Text>

                        <View style={{ flex: 1 }}>
                            <Text style={styles.headerLabel} >
                                What you need to pay monthly
                            </Text>
                            <Text style={styles.headerValue} >
                                VND 66 ngan
                            </Text>
                        </View>
                    </View>

                    <View style={styles.descriptionContainer}>
                        <View style={{ flex: 1 }} />

                        <View style={{ flex: 1 }}>
                            <Text style={styles.headerLabel} >
                                Protection Amount
                            </Text>
                            <Text style={styles.headerValue} >
                                VND 80 trieu
                            </Text>
                        </View>
                    </View>

                    <View style={styles.descriptionContainer}>
                        <View style={{ flex: 1 }}>
                            <Text style={styles.headerLabel}>
                                Insurer
                            </Text>
                            <Text style={styles.insureProvider}>
                                Bao Minh Corporation
                            </Text>
                        </View>

                        <View style={{ flex: 1 }}>
                            <Text style={styles.headerLabel}>
                                Amount of time you are covered
                            </Text>
                            <Text style={styles.headerValue}>
                                1 year
                            </Text>
                        </View>
                    </View>
                </View>

                <View style={styles.bottomTabContainer}>
                    <BottomTab
                        title={Messages.POLICIES}
                        icon={policy}
                        onTabPress={this.onTabSelected}
                        isSelected={selectedTab === Messages.POLICIES} />

                    <BottomTab
                        title={Messages.APPLY}
                        icon={apply}
                        onTabPress={this.onTabSelected}
                        isSelected={selectedTab === Messages.APPLY} />

                    <BottomTab
                        title={Messages.CLAIM}
                        icon={claim}
                        onTabPress={this.onTabSelected}
                        isSelected={selectedTab === Messages.CLAIM} />

                    <BottomTab
                        title={Messages.ACCOUNT}
                        icon={account}
                        onTabPress={this.onTabSelected}
                        isSelected={selectedTab === Messages.ACCOUNT} />
                </View>
            </SafeAreaView>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.DARK_GREEN
    },
    bottomTabContainer: {
        height: 56,
        backgroundColor: Colors.WHITE,
        flexDirection: 'row'
    },
    addMoreProtectionButton: {
        alignSelf: 'flex-start',
        marginTop: 16,
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 20,
        paddingVertical: 10,
        paddingHorizontal: 16,
        borderRadius: 25,
        borderWidth: 1,
        borderColor: Colors.WHITE
    },
    addMoreProtectionButtonLabel: {
        fontSize: 16,
        fontWeight: '700',
        color: Colors.WHITE
    },
    cardContainer: {
        flex: 1,
        marginHorizontal: 20,
        marginTop: 20,
        marginBottom: 40,
        backgroundColor: Colors.WHITE,
        borderRadius: 15,
        elevation: 2,
        padding: 16
    },
    expireLabel: {
        flex: 1,
        marginRight: 16,
        fontSize: 16,
        color: Colors.BLACK,
        fontWeight: '700'
    },
    arrowIcon: {
        width: 24,
        height: 24
    },
    expireContainer: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    paymentDueLabel: {
        fontSize: 14,
        color: Colors.GREEN,
        fontWeight: '700',
        marginTop: 10
    },
    paymentDueDate: {
        fontSize: 16,
        fontWeight: 'bold',
        color: Colors.GREEN
    },
    descriptionContainer: {
        flexDirection: 'row',
        alignItems: 'flex-end',
        marginTop: 20
    },
    singleHealthLabel: {
        lineHeight: 25,
        flex: 1,
        fontSize: 25,
        fontWeight: 'bold',
        color: Colors.BLACK
    },
    headerLabel: {
        fontSize: 12,
        color: Colors.BLACK
    },
    headerValue: {
        fontSize: 18,
        color: Colors.BLACK,
        fontWeight: '700'
    },
    insureProvider: {
        fontSize: 20,
        lineHeight: 20,
        color: Colors.BLACK,
        fontWeight: '700'
    }
})

export default Home;