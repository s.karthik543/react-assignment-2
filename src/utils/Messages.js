export default {
    POLICIES: 'Polices',
    APPLY: 'Apply',
    CLAIM: 'Claim',
    ACCOUNT: 'Account'
}