export default {
    WHITE: '#FFFFFF',
    BLACK: '#000000',
    DARK_GREEN: '#018a4f',
    YELLOW: '#efc859',
    GREEN: '#13835b'
}